window.onload = function(){
    var windowSize = document.documentElement.clientHeight
    var navHome = document.getElementById("navbar-home")
    var navAbout = document.getElementById("navbar-about")
    var navlilgirl = document.getElementById("navbar-lilgirl")
    var navHunter = document.getElementById("navbar-hunter")
    var navRegist = document.getElementById("navbar-registration")

    navHome.onclick = function() {
        window.scrollTo({
            top: 0 * windowSize,
            behavior: 'smooth'
        })
        return false;
    }
    navAbout.onclick = function() {
        window.scrollTo({
            top: 1 * windowSize,
            behavior: 'smooth'
        })
        return false;
    }
    navlilgirl.onclick = function() {
        window.scrollTo({
            top: 2 * windowSize,
            behavior: 'smooth'
        })
        return false;
    }
    navHunter.onclick = function() {
        window.scrollTo({
            top: 3 * windowSize,
            behavior: 'smooth'
        })
        return false;
    }
    navRegist.onclick = function() {
        window.scrollTo({
            top: 4 * windowSize,
            behavior: 'smooth'
        })
        return false;
    }

}